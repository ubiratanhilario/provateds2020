package com.ubh.provateds2020;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;


class Equacao2oGrauTest {
	public Equacao2oGrau equacao2ograu;
	@Test
	void TestZero() 
	 {
		equacao2ograu = new Equacao2oGrau(1,-0,-0);
		assertEquals(-0.0, equacao2ograu.getRaizes().getR1());
		assertEquals(-0.0, equacao2ograu.getRaizes().getR2());
	 }
	@Test
	void TestUnica() 
	 {
		equacao2ograu = new Equacao2oGrau(4,-4,1);
		assertEquals(0.5, equacao2ograu.getRaizes().getR1());
		assertEquals(0.5, equacao2ograu.getRaizes().getR2());
	 }
	 
	@Test
	void TestDuasRaizes() 
	 {
		equacao2ograu = new Equacao2oGrau(1,0,-9);
		assertEquals(3.0, equacao2ograu.getRaizes().getR1());
		assertEquals(-3.0, equacao2ograu.getRaizes().getR2());
	 }
	@Test
	void TestDuasRaizesCobreMutantes() 
	 {
		equacao2ograu = new Equacao2oGrau(3,-7,4);
		assertEquals(1.3333333333333333, equacao2ograu.getRaizes().getR1());
		assertEquals(1.0, equacao2ograu.getRaizes().getR2());
	 }
	 
	@Test
	void TestNaoharaizes() 
	 {
		equacao2ograu = new Equacao2oGrau(-1,0,-9);
		assertThrows(ArithmeticException.class, () -> equacao2ograu.getRaizes(), "Delta Negativo. Não existem raízes reais.");
	 }
	 
} 
